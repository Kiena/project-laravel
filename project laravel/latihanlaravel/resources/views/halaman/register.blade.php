@extends('layout.master')

@section('judul')
Halaman Biodata
@endsection

@section('content')
      <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br><br>
            <input type="text" name="nama"/><br><br>
          <label>Last name:</label><br><br>
                <input type="text" name="namaAkhir"/><br><br>
            <label>Gender:</label><br>
                <label><input type="radio" name="gender" value="Male"/>Male</label><br>
                <label><input type="radio" name="gender" value="Female"/>Female</label><br>
                <label><input type="radio" name="gender" value="Other"/>Other</label><br><br>
                  <label>Nationality:</label><br><br>
                      <select name="nationality">
                        <option value="id">Indonesia</option>
                        <option value="my">Malaysia</option>
                        <option value="sg">Singapore</option>
                        <option value="jp">Japan</option>
                      </select><br><br>
              <label>Language Spoken:</label><br>
                  <label><input type="checkbox" name="Language" value="id"/>Indonesia</label><br>
                  <label><input type="checkbox" name="Language" value="en"/>English</label><br>
                  <label><input type="checkbox" name="Language" value="Other"/>Other</label><br><br>
        <label>Bio:</label><br><br>
            <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" name="submit" value="Sign Up"/>
    </form>
@endsection