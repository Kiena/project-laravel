@extends('layout.master')

@section('judul')
Halaman Index
@endsection

@section('content')

    <h1>Media Online</h1>
    <H3>Sosial Media Developer</H3>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <H3>Benefit Join di Media Online</H3>
    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <H3>Cara Bergabung ke Media Online</H3>
    <ol>
        <li>Mengunjungi website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
    @endsection
